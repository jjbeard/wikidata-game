<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '../../php/common.php' ) ;

function getUID ( $db , $user ) {
	$uid = '' ;
	$sql = "SELECT * FROM users WHERE name='$user'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $uid = $o->id ;
	if ( $uid == '' ) {
		$sql = "INSERT IGNORE INTO users (name) VALUES ('$user')" ;
		$out['sql'][] = $sql ;
		$db->query($sql) ;
		$uid = $db->insert_id ;
		$sql = "INSERT INTO scores (user) VALUES ($uid)" ;
		$out['sql'][] = $sql ;
		$db->query($sql) ;
	}
	return $uid ;
}

function hasLink ( $db , $q , $target ) {
	$title = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
	$target = strtoupper ( $target ) ;
	$ns = 0 ;
	if ( $target[0] == 'P' ) $ns = 120 ;
	$sql = "SELECT * FROM page,pagelinks WHERE page_title='$title' AND page_namespace=0 AND page_id=pl_from AND pl_namespace=$ns AND pl_title='$target' LIMIT 1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()) return true ;
	return false ;
}

function isRedirect ( $db , $q ) {
	$title = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
	$sql = "SELECT * FROM page WHERE page_title='$title' AND page_namespace=0 AND page_is_redirect=1 LIMIT 1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()) return true ;
	return false ;
}

function isDeleted ( $db , $q ) {
	$title = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
	$sql = "SELECT * FROM page WHERE page_title='$title' AND page_namespace=0 LIMIT 1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()) return false ;
	return true ;
}

?>